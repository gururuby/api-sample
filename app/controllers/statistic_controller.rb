class StatisticController < ApplicationController
  def show
    @statistic = Statistic.get_by(params[:period])
  end
end
