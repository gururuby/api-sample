class API::V1::MessagesController < API::V1::BaseController
  def index
    expose Message.ordered
  end

  def create
    message = current_user.messages.build(message_params)
    if message.save
      expose(message)
    else
      expose(status: 422, errors: message.errors)
    end
  end

  private

  def message_params
    params.require(:message).permit(:content)
  end
end
