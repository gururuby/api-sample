module API
  module V1
    class BaseController < RocketPants::Base
      version 1

      before_action :authenticate_user_from_token
      before_action :require_authentication!

      attr_reader :current_user

      def require_authentication!
        error! :unauthenticated if current_user.nil?
      end

      def authenticate_user_from_token
        user = authenticate_with_http_token do |token, options|
          user_id = options['user_id']
          user = user_id && User.find_by_id(user_id)
          user if user && Devise.secure_compare(user.authentication_token, token)
        end
        @current_user = user
      end
    end
  end
end
