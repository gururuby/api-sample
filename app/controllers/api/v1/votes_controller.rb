module API
  module V1
    class VotesController < BaseController

      def create
        message = Message.find(vote_params[:message_id])
        if message.liked_by(current_user)
          expose(status: 200)
        else
          expose(status: 422)
        end
      end

      private

      def vote_params
        params.require(:vote).permit(:message_id)
      end
    end
  end
end
