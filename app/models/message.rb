class Message < ActiveRecord::Base
  belongs_to :user, counter_cache: true
  has_many :votes

  scope :ordered, -> { order(created_at: :desc) }

  def liked_by(user)
    votes.create(user_id: user.id)
  end
end
