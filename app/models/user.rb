class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :messages, dependent: :destroy
  has_many :votes, dependent: :destroy

  def to_s
    email
  end

  def generate_authentication_token
    loop do
      self.authentication_token = SecureRandom.hex
      break unless User.exists?(authentication_token: authentication_token)
    end
    save!
    authentication_token
  end
end
