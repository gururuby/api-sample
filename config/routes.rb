Rails.application.routes.draw do
  root to: 'statistic#show'

  devise_for :users

  namespace :api do
    api version: 1, module: 'v1' do
      resources :sessions, only: [:create]
      resources :messages, only: [:index, :create] do
        resources :votes, only: [:create]
      end
    end
  end

  resource :statistic, only: :show, constraint: {period: /week|day/}
end
