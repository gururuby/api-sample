class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.belongs_to :user, null: false
      t.belongs_to :message, null: false
      t.timestamps null: false
    end
    add_index :votes, [:user_id, :message_id], unique: true
  end
end
