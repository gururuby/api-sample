class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.belongs_to :user, index: true, null: false
      t.text :content, null: false
      t.integer :votes_count, default: 0
      t.timestamps null: false
    end
  end
end
