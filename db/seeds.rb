10.times do |count|
  User.find_or_create_by(email: "test@test#{count}.com") do |u|
    u.password = '12345678'
  end
end

20.times do |count|
  user_offset = rand(User.count)
  Message.create(created_at: count.day.ago, content: 'Test content', user_id: User.offset(user_offset).take.id)
end

30.times do
  user_offset = rand(User.count)
  message_offset = rand(Message.count)
  Message.offset(message_offset).take.liked_by(User.offset(user_offset).take)
end
