class Statistic

  def self.get_by(period)
    interval = case period
                 when 'week'
                   1.week.ago
                 when 'day'
                   1.day.ago
                 else
                   0
               end
    users_with_messages = User.joins(:messages).where('messages.created_at > ?', interval).group('users.id').limit(5)
    {
        by_messages_count: users_with_messages.select('*, count(messages.id) as messages_counter').order('messages_counter DESC'),
        by_votes_count: users_with_messages.joins(messages: :votes)
                            .select('*, count(votes.id) as votes_count, max(votes_count) as max_votes')
                            .order('max_votes DESC'),
        by_average_votes: users_with_messages.joins(messages: :votes).select('*, count(votes.id) as votes_count, avg(votes_count) as average_votes')
                              .order('average_votes DESC')
    }
  end

end
