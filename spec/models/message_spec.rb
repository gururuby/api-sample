require 'rails_helper'

RSpec.describe Message, type: :model do

  let(:user) { create(:user) }
  let(:message) { create(:message) }

  context '#liked_by' do

    it 'success' do
      vote = message.liked_by(user)
      expect(vote).to eq(Vote.last)
    end

    it 'failed' do

    end
  end

end
